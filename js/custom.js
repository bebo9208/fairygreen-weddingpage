var letszamDiv = document.getElementById("letszamDiv");
var letszamSelect = document.getElementById("letszam");

function handleSzallasRadio() {
  var szallasRadio = document.querySelector('input[name="szallas"]:checked');

  if (szallasRadio && szallasRadio.value === "igen") {
    letszamDiv.style.display = "block";
    // Alapértelmezett érték visszaállítása
    letszamSelect.selectedIndex = 0;
  } else {
    letszamDiv.style.display = "none";
  }
}

document.addEventListener("DOMContentLoaded", function () {
  var nemSzallasRadio = document.querySelector('input[value="nem"]');
  if (nemSzallasRadio) {
    nemSzallasRadio.addEventListener("change", function () {
      letszamDiv.style.display = "none";
    });
  }
});



function addName() {
  var nameContainer = document.getElementById("nameContainer");

  var newInputContainer = document.createElement("div");
  newInputContainer.classList.add("inputContainer");

  var newInput = document.createElement("input");
  newInput.type = "text";
  newInput.id = "name" + Date.now();
  newInput.name = "name[]";
  newInput.required = true;
  newInput.placeholder = "Teljes név";
  newInput.classList.add("newInputClass");

  // Létrehozunk egy törlő gombot és egy span elemet a gombnak
  var removeButton = document.createElement("span");
  removeButton.textContent = "X";
  removeButton.classList.add("removeButton");
  removeButton.onclick = function() {
    // Ezzel eltávolítjuk a div-et, amely tartalmazza az input mezőt és a törlő gombot
    nameContainer.removeChild(newInputContainer);
  };

  newInputContainer.appendChild(newInput);
  newInputContainer.appendChild(removeButton);

  // Az új inputContainer (input + törlő gomb) hozzáadása a nameContainer-hoz
  nameContainer.appendChild(newInputContainer);
}


document.addEventListener("DOMContentLoaded", function () {
  // Esküvői dátum beállítása (év, hónap - 1, nap, óra, perc, másodperc)
  const weddingDate = new Date(2024, 4, 4, 16, 30, 0); // A hónapok 0-tól kezdődnek (január = 0, február = 1, stb.)

  function updateCountdown() {
    const currentDate = new Date();
    const timeDifference = weddingDate - currentDate;

    const days = Math.floor(timeDifference / (1000 * 60 * 60 * 24));
    const hours = Math.floor(
      (timeDifference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    const minutes = Math.floor(
      (timeDifference % (1000 * 60 * 60)) / (1000 * 60)
    );
    const seconds = Math.floor((timeDifference % (1000 * 60)) / 1000);

    document.getElementById("days").textContent = formatTime(days);
    document.getElementById("hours").textContent = formatTime(hours);
    document.getElementById("minutes").textContent = formatTime(minutes);
    document.getElementById("seconds").textContent = formatTime(seconds);
  }

  function formatTime(time) {
    return time < 10 ? `0${time}` : time;
  }

  setInterval(updateCountdown, 1000);
});

document.addEventListener("DOMContentLoaded", function () {
  const carousel = document.querySelector(".carousel");
  const imgs = carousel.querySelectorAll("img");
  let imgWidth = 0;
  let imgMarginRight = 0;

  if (imgs.length > 0) {
    const imgStyle = window.getComputedStyle(imgs[0]);
    imgWidth = imgs[0].offsetWidth;
    imgMarginRight = parseInt(imgStyle.marginRight, 10);
  }

  const scrollToImage = (direction) => {
    const currentScroll = carousel.scrollLeft;
    const carouselWidth = carousel.offsetWidth;
    
    const currentCenter = currentScroll + carouselWidth / 2;
    let closestIndex = 0;
    let smallestDistance = Infinity;

    imgs.forEach((img, index) => {
      const imgCenter = img.offsetLeft + img.offsetWidth / 2;
      const distance = Math.abs(currentCenter - imgCenter);
      if (distance < smallestDistance) {
        closestIndex = index;
        smallestDistance = distance;
      }
    });

    let targetIndex = closestIndex + direction;
    targetIndex = Math.max(0, Math.min(imgs.length - 1, targetIndex));

    const targetImg = imgs[targetIndex];
    const targetPosition = targetImg.offsetLeft - (carouselWidth / 2) + (targetImg.offsetWidth / 2);

    carousel.scrollTo({ left: targetPosition, behavior: "smooth" });
  };

  const prevButton = document.querySelector(".fa-chevron-left");
  const nextButton = document.querySelector(".fa-chevron-right");

  prevButton.addEventListener("click", () => scrollToImage(-1));
  nextButton.addEventListener("click", () => scrollToImage(1));


  // Érintőképernyő kezelése
  let isTouching = false;
  let startX;
  let scrollStartX;

  const touchStart = (e) => {
    isTouching = true;
    startX = e.touches[0].pageX;
    scrollStartX = carousel.scrollLeft;
  };

  const touchMove = (e) => {
    if (!isTouching) return;
    const currentX = e.touches[0].pageX;
    carousel.scrollLeft = scrollStartX - (currentX - startX);
  };
  const touchEnd = (e) => {
    if (!isTouching) return;

    const movedX = carousel.scrollLeft - scrollStartX;
    const direction = movedX > 0 ? 1 : -1;
    const absMovedX = Math.abs(movedX);
    let targetIndex;

    const visibleImgs = Array.from(imgs).map((img) => {
      const imgLeft = img.offsetLeft - carousel.scrollLeft;
      const imgRight = imgLeft + img.offsetWidth;
      const visibleWidth = Math.max(
        0,
        Math.min(imgRight, carousel.offsetWidth) - Math.max(0, imgLeft)
      );
      return { img, visibleWidth };
    });

    visibleImgs.sort((a, b) => b.visibleWidth - a.visibleWidth);
    const mostVisibleImg = visibleImgs[0].img;

    if (absMovedX > 0) {
      targetIndex = Array.from(imgs).indexOf(mostVisibleImg) + direction;
    } else {
      targetIndex = Array.from(imgs).indexOf(mostVisibleImg);
    }

    targetIndex = Math.max(0, Math.min(imgs.length - 1, targetIndex));
    let targetPosition =
      imgs[targetIndex].offsetLeft -
      carousel.offsetWidth / 2 +
      imgs[targetIndex].offsetWidth / 2;

    carousel.scrollTo({ left: targetPosition, behavior: "smooth" });

    isTouching = false;
  };

  carousel.addEventListener("touchend", touchEnd);
  carousel.addEventListener("touchstart", touchStart);
  carousel.addEventListener("touchmove", touchMove);

  // Modális ablak a képekhez
  const modal = document.createElement("div");
  modal.style.cssText =
    "position:fixed; top:0; left:0; width:100%; height:100%; background:rgba(0,0,0,0.8); display:none; justify-content:center; align-items:center; z-index:1000;";
  document.body.appendChild(modal);

  const modalImg = document.createElement("img");
  modalImg.style.maxWidth = "80%";
  modalImg.style.maxHeight = "80%";
  modal.appendChild(modalImg);

  // X gomb hozzáadása a modal ablakhoz
  const closeButton = document.createElement("span");
  closeButton.innerHTML = "&times;";
  closeButton.style.position = "absolute";
  closeButton.style.top = "60px";
  closeButton.style.right = "20px";
  closeButton.style.cursor = "pointer";
  closeButton.style.fontSize = "56px";
  closeButton.style.fontWeight = "700";
  modal.appendChild(closeButton);

  imgs.forEach((img) => {
    img.addEventListener("click", function () {
      modalImg.src = this.src;
      modal.style.display = "flex";
    });
  });

  // Modal ablak bezárása az "x" gombra kattintva
  closeButton.addEventListener("click", () => (modal.style.display = "none"));

  modal.addEventListener("click", () => (modal.style.display = "none"));
});

document.addEventListener("DOMContentLoaded", function () {
  // Kattintás eseménykezelő hozzáadása minden menüpont linkhez
  const navLinks = document.querySelectorAll("nav ul li a");

  navLinks.forEach((link) => {
    link.addEventListener("click", function (event) {
      event.preventDefault();

      const targetId = this.getAttribute("href").substring(1);
      const targetElement = document.getElementById(targetId);

      if (targetElement) {
        const offset = 120;
        const targetPosition = targetElement.offsetTop - offset;
        const scrollDuration = 1000;

        // Görgetés animálása

        window.scrollTo({
          top: targetPosition,
          behavior: "smooth",
          duration: scrollDuration,
        });
      }
    });
  });

  // map
  let map;

  async function initMap() {
    // The location
    const position = { lat: 47.337889, lng: 19.419055 };
    // Request needed libraries.
    //@ts-ignore
    const { Map } = await google.maps.importLibrary("maps");
    const { AdvancedMarkerElement } = await google.maps.importLibrary("marker");

    // The map, centered at location
    map = new Map(document.getElementById("map"), {
      zoom: 10,
      center: position,
      mapId: "da01c4841e978679",
    });

    // The marker
    const marker = new AdvancedMarkerElement({
      map: map,
      position: position,
      title: "Helyszín",
    });
  }

  initMap();
});
